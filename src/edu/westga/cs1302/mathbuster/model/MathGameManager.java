/**
 * 
 */
package edu.westga.cs1302.mathbuster.model;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import edu.westga.cs1302.mathbuster.view.resourses.UI;

/**
 * The class MathGameManager.
 * 
 * @author Aaron Merrell
 *
 */
public class MathGameManager implements Collection<MathGamePlayer> {
	private ArrayList<MathGamePlayer> players;
	
	/**
	 * Instantiates a new math game manager.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public MathGameManager() {
		this.players = this.loadPlayers();
		this.sort(this.players);
	}
	
	private ArrayList<MathGamePlayer> loadPlayers() {
		ArrayList<MathGamePlayer> thePlayers = new ArrayList<MathGamePlayer>();
		try (Scanner scan = new Scanner(new File(UI.Utility.PLAYER_FILE))) {
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				MathGamePlayer player = this.generatePlayer(this.parseLine(line, ",|\\n|\\r\\n"));
				if (player != null) {
					thePlayers.add(player);
				}
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		
		this.sort(thePlayers);
		
		return thePlayers;
	}
	
	/**
	 * Saves the players to a text file.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void savePlayers() {
		this.sort(this.players);
		try (PrintWriter writer = new PrintWriter(UI.Utility.PLAYER_FILE)) {
			for (MathGamePlayer currPlayer : this.players) {
				String output = currPlayer.getName() + "," + currPlayer.getScore();
				writer.println(output);
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
	private String[] parseLine(String line, String delimiter) {
		return line.split(delimiter);
	}
	
	private MathGamePlayer generatePlayer(String[] arg0) {
		MathGamePlayer player;
		
		if (arg0.length == 2) {
			String name = arg0[0];
			name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
			player = new MathGamePlayer(name, Integer.parseInt(arg0[1]));
		} else {
			player = null;
		}
		
		
		return player;
	}
	
	private void sort(List<MathGamePlayer> collection) {
		Collections.sort(collection, new Comparator<MathGamePlayer>() {
			@Override
			public int compare(MathGamePlayer player0, MathGamePlayer player1) {
				if (player0.getScore() == player1.getScore()) {
					return player0.getName().compareTo(player1.getName());
				} else {
					return player1.getScore() - player0.getScore();
				}
			}
		});
	}
	
	private boolean updatePlayer(String name, int score) {
		boolean result = false;
		for (MathGamePlayer currPlayer : this.players) {
			if (currPlayer.getName().equalsIgnoreCase(name) && currPlayer.getScore() < score) {
				currPlayer.setName(name);
				currPlayer.setScore(score);
				result = true;
				break;
			}
		}
		this.sort(this.players);
		return result;
	}
	
	/**
	 * Updates the player list.
	 * 
	 * @precondition none
	 * @postcondition updates a current player or adds a new one
	 * 
	 * @param name the name
	 * @param score the score
	 */
	public void update(String name, int score) {
		MathGamePlayer player = new MathGamePlayer(name, score);
		if (!this.updatePlayer(name, score) && !this.contains(player)) {
			this.add(player);
			this.sort(this.players);
		}
	}
	
	private boolean contains(MathGamePlayer player) {
		for (MathGamePlayer currPlayer : this.players) {
			if (player.getName().equalsIgnoreCase(currPlayer.getName())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean add(MathGamePlayer arg0) {
		
		return this.players.add(arg0);
	}

	@Override
	public boolean addAll(Collection<? extends MathGamePlayer> arg0) {
		
		return this.players.addAll(arg0);
	}

	@Override
	public void clear() {
		this.players.clear();
		
	}

	@Override
	public boolean contains(Object arg0) {
		
		return this.players.contains(arg0);
	}

	@Override
	public boolean containsAll(Collection<?> arg0) {
		
		return this.players.containsAll(arg0);
	}

	@Override
	public boolean isEmpty() {
		
		return this.players.isEmpty();
	}

	@Override
	public Iterator<MathGamePlayer> iterator() {
		
		return this.players.iterator();
	}

	@Override
	public boolean remove(Object arg0) {
		return this.players.remove(arg0);
	}

	@Override
	public boolean removeAll(Collection<?> arg0) {
		
		return this.players.removeAll(arg0);
	}

	@Override
	public boolean retainAll(Collection<?> arg0) {
		
		return this.players.retainAll(arg0);
	}

	@Override
	public int size() {
		
		return this.players.size();
	}

	@Override
	public Object[] toArray() {
		
		return this.players.toArray();
	}

	@Override
	public <T> T[] toArray(T[] arg0) {
		
		return this.players.toArray(arg0);
	}
}
