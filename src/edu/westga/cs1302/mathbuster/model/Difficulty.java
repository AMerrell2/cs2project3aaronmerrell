/**
 * 
 */
package edu.westga.cs1302.mathbuster.model;

/**
 * Defines the levels of difficulty.
 * 
 * @author Aaron Merrell
 *
 */
public enum Difficulty {
	EASY, MEDIUM, HARD;
}
