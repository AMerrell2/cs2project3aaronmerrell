/**
 * 
 */
package edu.westga.cs1302.mathbuster.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.Line;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;

import edu.westga.cs1302.mathbuster.view.resourses.UI;

/**
 * The class MathGame.
 * 
 * @author Aaron Merrell
 *
 */
public class MathGamePlayer implements Comparable<MathGamePlayer> {
	private String name;
	private int score;
	private int operand0;
	private int operand1;
	private String mathPropblem;
	private boolean isCorrectlySolved;
	private boolean correctSubmission;
	private int numberGeneratedProblems;
	private ArrayList<String> answerFeedback;
	private Random rand;
	private Difficulty difficulty;

	/**
	 * Instantiates a new math game.
	 * 
	 * @precondition none
	 * @postcondition getScore() == 0 && getNumberGeneratedProblems() == 1
	 * 
	 * 
	 */
	public MathGamePlayer() {
		this.rand = new Random();
		this.name = "";
		this.operand0 = 0;
		this.operand1 = 0;
		this.score = 0;
		this.correctSubmission = false;
		this.numberGeneratedProblems = 1;
		this.answerFeedback = this.loadFeedback();
		this.isCorrectlySolved = false;
	}

	/**
	 * Instantiates a player with a name and score.
	 * 
	 * @precondition name is not empty.
	 * @postcondition getName() == name && getScore() == score
	 * 
	 * @param name  the name
	 * @param score the score
	 */
	public MathGamePlayer(String name, int score) {
		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.EMPTY_NAME);
		}
		this.name = name;
		this.score = score;
	}

	private ArrayList<String> loadFeedback() {
		ArrayList<String> strings = new ArrayList<String>();
		try (Scanner scan = new Scanner(new File("TextAudioFiles/AnswerFeedback.txt"))) {
			while (scan.hasNextLine()) {
				strings.add(scan.nextLine());
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return strings;
	}

	

	private void buildMathProblem(boolean operator) {
		if (operator) {
			this.mathPropblem = this.operand0 + UI.Utility.PLUS_SIGN + this.operand1 + UI.Utility.EQUAL_SIGN;
		} else {
			this.mathPropblem = this.operand0 + UI.Utility.MINUS_SIGN + this.operand1 + UI.Utility.EQUAL_SIGN;
		}
	}

	/**
	 * Gets the player name.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name
	 * 
	 * @precondition name is not empty.
	 * @postcondition getName() == name.
	 * 
	 * @param name the name to set.
	 */
	public void setName(String name) {
		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.EMPTY_NAME);
		}
		this.name = name;
	}

	/**
	 * Gets if the submission is correct or not.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the correctSubmission
	 */
	public boolean isCorrectSubmission() {
		return this.correctSubmission;
	}

	/**
	 * Sets if the submission is correct or not.
	 * 
	 * @precondition none
	 * @postcondition isCorrectSubmission() == correctSubmission
	 * @param correctSubmission the correctSubmission to set
	 */
	public void setCorrectSubmission(boolean correctSubmission) {
		this.correctSubmission = correctSubmission;
	}

	/**
	 * Gets the score.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the score
	 */
	public int getScore() {
		return this.score;
	}
	
	/**
	 * Sets the score.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * Gets the math problem.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the mathPropblem
	 */
	public String getMathPropblem() {
		return this.mathPropblem;
	}

	/**
	 * Gets the number of generated math problems.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the numberGeneratedProblems
	 */
	public int getNumberGeneratedProblems() {
		return this.numberGeneratedProblems;
	}

	/**
	 * Gets operand0.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the operand0
	 */
	public int getOperand0() {
		return this.operand0;
	}

	/**
	 * Sets operand0.
	 * 
	 * @precondition operand0 > 0 && operand0 < 100.
	 * @postcondition getOperand0() == operand0.
	 * 
	 * @param operand0 the operand0 to set
	 */
	public void setOperand0(int operand0) {
		if (operand0 < 0 || operand0 > 1000) {
			throw new IllegalArgumentException(UI.ExceptionMessages.OPERAND_OUT_OF_BOUNDS);
		}
		this.operand0 = operand0;
	}

	/**
	 * Gets the operand1.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the operand1
	 */
	public int getOperand1() {
		return this.operand1;
	}

	/**
	 * Sets the operand1.
	 * 
	 * @precondition operand0 > 0 && operand0 < 100.
	 * @postcondition getOperand1() == operand1.
	 * 
	 * @param operand1 the operand1 to set
	 */
	public void setOperand1(int operand1) {
		if (operand1 < 0 || operand1 > 1000) {
			throw new IllegalArgumentException(UI.ExceptionMessages.OPERAND_OUT_OF_BOUNDS);
		}
		this.operand1 = operand1;
	}

	/**
	 * Gets if the current problem is solved.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true if solved, false otherwise
	 */
	public boolean isCorrectlySolved() {
		return this.isCorrectlySolved;
	}

	/**
	 * Sets if the problem has been solved.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param isCorrectlySolved the value to set
	 */
	public void setCorrectlySolved(boolean isCorrectlySolved) {
		this.isCorrectlySolved = isCorrectlySolved;
	}

	/**
	 * Gets the difficulty
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the difficulty
	 */
	public Difficulty getDifficulty() {
		return this.difficulty;
	}

	/**
	 * Sets the difficulty
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param difficulty the difficulty to set
	 */
	public void setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
	}

	/**
	 * checks if the user answer is correct.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param answer the user answer.
	 */
	public void checkAnswer(int answer) {
		if (this.getMathPropblem().contains("-")) {
			this.setCorrectSubmission((this.getOperand0() - this.getOperand1()) == answer);
		} else {
			this.setCorrectSubmission((this.getOperand0() + this.getOperand1()) == answer);
		}
		this.updateScore();
		if (this.isCorrectSubmission()) {
			this.setCorrectlySolved(true);
		}
	}

	/**
	 * Gets the answer feedback.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the answer feedback.
	 */
	public String getAnswerFeedbackMessage() {
		String feedback = "";
		if (this.isCorrectSubmission()) {
			while (!feedback.startsWith("+")) {
				feedback = this.answerFeedback.get(this.rand.nextInt(this.answerFeedback.size()));
			}
			this.play("TextAudioFiles/Ovation-Mike_Koenig-1061486511.wav");
		} else {
			while (!feedback.startsWith("-")) {
				feedback = this.answerFeedback.get(this.rand.nextInt(this.answerFeedback.size()));

			}
		}

		return feedback.substring(1, feedback.length());
	}

	/**
	 * Updates the score if the answer is correct.
	 * 
	 * @precondition none
	 * @postcondition if correct; getScore() @prev + 1, otherwise remains the same.
	 */
	private void updateScore() {
		if (this.isCorrectSubmission() && !this.isCorrectlySolved()) {
			if (this.difficulty == Difficulty.EASY) {
				this.score++;
			} else if (this.difficulty == Difficulty.MEDIUM) {
				this.score += 2;
			} else if (this.difficulty == Difficulty.HARD) {
				this.score += 3;
			}
		} else if (!this.isCorrectSubmission() && !this.isCorrectlySolved()) {
			int aScore = this.getScore();
			if (this.difficulty == Difficulty.EASY) {
				aScore -= 1;
			} else if (this.difficulty == Difficulty.MEDIUM) {
				aScore -= 2;
			} else if (this.difficulty == Difficulty.HARD) {
				aScore -= 3;
			}
			this.score = (aScore < 0) ? 0 : aScore;
		}
	}
	
	/**
	 * updates fields for the next problem.
	 * 
	 * @precondition none 
	 * @postcondition getNumberGeneratedProblems() @prev + 1
	 */
	public void updateForNextProblem() {
		this.setCorrectlySolved(false);
		this.setCorrectSubmission(false);
		this.numberGeneratedProblems++;
	}

	/**
	 * Generates a new math problem.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the new math problem
	 */
	public String generateNewMathProblem() {
		int op0 = 1000;
		int op1 = 2000;
		String problem = "";
		if (this.difficulty == Difficulty.EASY) {
			problem = this.generateEasyProblem(op0, op1);
		} else if (this.difficulty == Difficulty.MEDIUM) {
			problem = this.generateMediumProblem(op0, op1);
		} else if (this.difficulty == Difficulty.HARD) {
			problem = this.generateHardProblem(op0, op1);
		}
		return problem;
	}

	private String generateHardProblem(int op0, int op1) {
		String problem;
		boolean operator = this.rand.nextBoolean();
		if (operator) {
			while ((op0 + op1) >= 1000) {
				op0 = this.rand.nextInt(1000);
				op1 = this.rand.nextInt(1000);
			}
			this.setOperand0(op0);
			this.setOperand1(op1);
			this.buildMathProblem(operator);
			problem = this.getMathPropblem();
		} else {
			while ((op0 - op1) <= 0) {
				op0 = this.rand.nextInt(1000);
				op1 = this.rand.nextInt(1000);
			}
			this.setOperand0(op0);
			this.setOperand1(op1);
			this.buildMathProblem(operator);
			problem = this.getMathPropblem();
		}
		return problem;
	}

	private String generateMediumProblem(int op0, int op1) {
		String problem;
		while ((op0 + op1) >= 1000) {
			op0 = this.rand.nextInt(900) + 100;
			op1 = this.rand.nextInt(900) + 100;
		}
		this.setOperand0(op0);
		this.setOperand1(op1);
		this.buildMathProblem(true);
		problem = this.getMathPropblem();
		return problem;
	}

	private String generateEasyProblem(int op0, int op1) {
		String problem;
		while ((op0 + op1) >= 100) {
			op0 = this.rand.nextInt(100);
			op1 = this.rand.nextInt(100);
		}
		this.setOperand0(op0);
		this.setOperand1(op1);
		this.buildMathProblem(true);
		problem = this.getMathPropblem();
		return problem;
	}

	/**
	 * Determines if the session is over.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true if session is over, false otherwise.
	 */
	public boolean isSessionOver() {
		return (this.getNumberGeneratedProblems() <= 10) ? false : true;
	}
	
	private void play(String fileName) {
	    try {
	        final Clip clip = (Clip) AudioSystem.getLine(new Line.Info(Clip.class));

	        clip.addLineListener(new LineListener() {
	            @Override
	            public void update(LineEvent event) {
	                if (event.getType() == LineEvent.Type.STOP) {
	                    clip.close();
	                }
	            }
	        });

	        clip.open(AudioSystem.getAudioInputStream(new File(fileName)));
	        clip.start();
	    } catch (Exception e) {
	        e.printStackTrace(System.err);
	    }
	}

	@Override
	public String toString() {
		return this.getName() + " " + this.getScore();
	}

	@Override
	public int compareTo(MathGamePlayer arg0) {
		return arg0.getScore() - this.getScore();
	}
}
