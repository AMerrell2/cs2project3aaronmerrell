package edu.westga.cs1302.mathbuster;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;

/**
 * The class main.
 * 
 * @author Aaron Merrell
 *
 */
public class Main extends Application {
	private static final String WINDOW_TITLE = "Math Buster by Aaron Merrell";
	private static final String GUI_FXML = "view/MathBusterGui.fxml";
	
	@Override
	public void start(Stage primaryStage) {
		try {
			Pane pane = this.loadGui();
			Scene scene = new Scene(pane);
			primaryStage.setScene(scene);
			primaryStage.setTitle(WINDOW_TITLE);
			primaryStage.setAlwaysOnTop(true);
			primaryStage.show();
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Loads the GUI from the given FXML file.
	 * 
	 * @return the pane to be loaded
	 * @throws IOException
	 */
	private Pane loadGui() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource(GUI_FXML));
		return (Pane) loader.load();
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
