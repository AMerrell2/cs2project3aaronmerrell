/**
 * 
 */
package edu.westga.cs1302.mathbuster.viewmodel;

import edu.westga.cs1302.mathbuster.model.Difficulty;
import edu.westga.cs1302.mathbuster.model.MathGameManager;
import edu.westga.cs1302.mathbuster.model.MathGamePlayer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

/**
 * The viewmodel for the application.
 * 
 * @author Aaron Merrell
 *
 */
public class MathBusterViewModel {
	private StringProperty nameProperty;
	private BooleanProperty emptyNameProperty;
	private StringProperty mathProblemProperty;
	private IntegerProperty solutionProperty;
	private StringProperty feedbackProperty;
	private StringProperty playerNameProperty;
	private IntegerProperty scoreProperty;
	private BooleanProperty sessionEndProperty;
	private ListProperty<MathGamePlayer> playerListProperty;
	private ObjectProperty<MathGamePlayer> selectedPlayerProperty;
	private ListProperty<Difficulty> difficultyListProperty;
	private ObjectProperty<Difficulty> selectedDifficultyProperty;
	
	private MathGamePlayer game;
	private MathGameManager manager;
	
	/**
	 * Instantiates a new view model.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public MathBusterViewModel() {
		this.manager = new MathGameManager();
		this.nameProperty = new SimpleStringProperty();
		this.emptyNameProperty = new SimpleBooleanProperty();
		this.mathProblemProperty = new SimpleStringProperty();
		this.solutionProperty = new SimpleIntegerProperty();
		this.feedbackProperty = new SimpleStringProperty();
		this.playerNameProperty = new SimpleStringProperty();
		this.scoreProperty = new SimpleIntegerProperty();
		this.sessionEndProperty = new SimpleBooleanProperty();
		this.selectedPlayerProperty = new SimpleObjectProperty<MathGamePlayer>();
		this.playerListProperty = new SimpleListProperty<MathGamePlayer>(FXCollections.observableArrayList(this.manager));
		this.difficultyListProperty = new SimpleListProperty<Difficulty>(FXCollections.observableArrayList(Difficulty.values()));
		this.selectedDifficultyProperty = new SimpleObjectProperty<Difficulty>();
	}
	
	/**
	 * Gets the name property.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the nameProperty
	 */
	public StringProperty getNameProperty() {
		return this.nameProperty;
	}



	/**
	 * Gets the math problem property.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the mathProblemProperty
	 */
	public StringProperty getMathProblemProperty() {
		return this.mathProblemProperty;
	}



	/**
	 * Gets the solution property.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the solutionProperty
	 */
	public IntegerProperty getSolutionProperty() {
		return this.solutionProperty;
	}



	/**
	 * Gets the feedback property.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the feedbackProperty
	 */
	public StringProperty getFeedbackProperty() {
		return this.feedbackProperty;
	}



	/**
	 * Gets the player name property.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the playerNameProperty
	 */
	public StringProperty getPlayerNameProperty() {
		return this.playerNameProperty;
	}



	/**
	 * Gets the score property.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the scoreProperty
	 */
	public IntegerProperty getScoreProperty() {
		return this.scoreProperty;
	}

	/**
	 * Gets the empty name property.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the emptyNameProperty
	 */
	public BooleanProperty getEmptyNameProperty() {
		return this.emptyNameProperty;
	}

	/**
	 * Gets the session end property.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the sessionEndProperty
	 */
	public BooleanProperty getSessionEndProperty() {
		return this.sessionEndProperty;
	}

	/**
	 * Gets the player list property.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the playerListProperty
	 */
	public ListProperty<MathGamePlayer> getPlayerListProperty() {
		return this.playerListProperty;
	}

	/**
	 * Gets the selected player property.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the selectedPlayerProperty
	 */
	public ObjectProperty<MathGamePlayer> getSelectedPlayerProperty() {
		return this.selectedPlayerProperty;
	}

	/**
	 * Gets the difficultyListProperty
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the difficultyListProperty
	 */
	public ListProperty<Difficulty> getDifficultyListProperty() {
		return this.difficultyListProperty;
	}

	/**
	 * Gets the selectedDifficultyProperty
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the selectedDifficultyProperty
	 */
	public ObjectProperty<Difficulty> getSelectedDifficultyProperty() {
		return this.selectedDifficultyProperty;
	}

	/**
	 * Initializes the UI fields.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void initializeFields() {
		if (this.nameProperty.get() == null || this.nameProperty.get().isEmpty()) {
			this.emptyNameProperty.set(true);
		} else {
			this.emptyNameProperty.set(false);
			this.game = new MathGamePlayer();
			this.game.setName(this.nameProperty.get());
			this.playerNameProperty.set(this.game.getName() + "'s Score");
			this.scoreProperty.set(this.game.getScore());
			this.feedbackProperty.set("");
			this.sessionEndProperty.set(this.game.isSessionOver());
		}
		if (this.selectedDifficultyProperty != null) {
			this.game.setDifficulty(this.selectedDifficultyProperty.get());
			this.game.generateNewMathProblem();
			this.mathProblemProperty.set(this.game.getMathPropblem());
		}
	}
	
	/**
	 * Submits user input to be calculated.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void submit() {
		this.game.checkAnswer(this.solutionProperty.get());
		this.feedbackProperty.set(this.game.getAnswerFeedbackMessage());
		this.scoreProperty.set(this.game.getScore());
	}
	
	/**
	 * Gets the next math problem for the UI.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void next() {
		this.game.generateNewMathProblem();
		this.game.updateForNextProblem();
		this.sessionEndProperty.set(this.game.isSessionOver());
		if (!this.game.isSessionOver()) {
			this.mathProblemProperty.set(this.game.getMathPropblem());
		} else {
			this.manager.update(this.nameProperty.get(), this.scoreProperty.get());
			this.playerListProperty.set(FXCollections.observableArrayList(this.manager));
		}
		this.feedbackProperty.set("");

	}
	
	/**
	 * Saves the players to a text file.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void save() {
		this.manager.savePlayers();
	}
	
	/**
	 * sets the difficulty for the problem.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void setDifficulty() {
		this.game.setDifficulty(this.selectedDifficultyProperty.get());
		this.game.generateNewMathProblem();
		this.mathProblemProperty.set(this.game.getMathPropblem());
	}
}
