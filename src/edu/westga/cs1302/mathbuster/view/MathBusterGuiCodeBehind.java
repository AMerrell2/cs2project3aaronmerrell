/**
 * 
 */
package edu.westga.cs1302.mathbuster.view;

import edu.westga.cs1302.mathbuster.model.Difficulty;
import edu.westga.cs1302.mathbuster.model.MathGamePlayer;
import edu.westga.cs1302.mathbuster.viewmodel.MathBusterViewModel;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;

/**
 * The code behind for the math buster application.
 * 
 * @author Aaron Merrell
 *
 */
public class MathBusterGuiCodeBehind {

	@FXML
	private TextField nameTextField;

	@FXML
	private Button startButton;

	@FXML
	private Label additionProblemLabel;

	@FXML
	private TextField solutionTextField;

	@FXML
	private Label answerFeedbackLabel;

	@FXML
	private Label missingNameLabel;

	@FXML
	private Button submitButton;

	@FXML
	private Button nextButton;

	@FXML
	private Label playerNameLabel;

	@FXML
	private Label scoreLabel;
	
	@FXML
    private Button saveButton;
	
	@FXML
    private ListView<MathGamePlayer> playerListView;
	
	@FXML
    private ComboBox<Difficulty> difficultyComboBox;


	
	private MathBusterViewModel viewModel;
	private ObjectProperty<MathGamePlayer> selectedPlayer;
	
	/**
	 * Instantiates a MathBusterGuiCodeBehind.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public MathBusterGuiCodeBehind() {
		this.viewModel = new MathBusterViewModel();
		this.selectedPlayer = new SimpleObjectProperty<MathGamePlayer>();
	}
	
	@FXML
	private void initialize() {
		this.playerListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		this.nameTextField.setPromptText("Please enter a name.");
		this.setupBindings();
		this.setupDisableBindings();
		this.setupListenersForValidation();
		this.setupListenersForListView();
		this.setupSelectionHandler();
		this.solutionTextField.setText("");
	}
	
	private void setupBindings() {
		this.nameTextField.textProperty().bindBidirectional(this.viewModel.getNameProperty());
		this.additionProblemLabel.textProperty().bind(this.viewModel.getMathProblemProperty());
		this.missingNameLabel.visibleProperty().bind(this.viewModel.getEmptyNameProperty());
		this.answerFeedbackLabel.textProperty().bindBidirectional(this.viewModel.getFeedbackProperty());
		this.solutionTextField.textProperty().bindBidirectional(this.viewModel.getSolutionProperty(), new NumberStringConverter());
		this.playerNameLabel.textProperty().bindBidirectional(this.viewModel.getPlayerNameProperty());
		this.scoreLabel.textProperty().bindBidirectional(this.viewModel.getScoreProperty(), new NumberStringConverter());
		this.selectedPlayer.bindBidirectional(this.viewModel.getSelectedPlayerProperty());
		this.playerListView.itemsProperty().bind(this.viewModel.getPlayerListProperty());
		this.difficultyComboBox.itemsProperty().bind(this.viewModel.getDifficultyListProperty());
		this.viewModel.getSelectedDifficultyProperty().bind(this.difficultyComboBox.getSelectionModel().selectedItemProperty());
	}
	
	private void setupDisableBindings() {
		this.submitButton.disableProperty().bind(this.playerNameLabel.textProperty().isEmpty().or(this.viewModel.getSessionEndProperty()));
		this.nextButton.disableProperty().bind(this.playerNameLabel.textProperty().isEmpty().or(this.viewModel.getSessionEndProperty()));
		this.difficultyComboBox.disableProperty().bind(this.playerNameLabel.textProperty().isEmpty().or(this.viewModel.getSessionEndProperty()));
		this.saveButton.disableProperty().bind(this.playerNameLabel.textProperty().isEmpty());
	}
	
	private void setupListenersForValidation() {
		this.nameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue.matches("|([A-Za-z])+")) {
				this.nameTextField.setText(oldValue);
			} else if (newValue.matches("[A-Za-z]{1}")) {
				this.nameTextField.setText(newValue.substring(0, 1).toUpperCase() + newValue.substring(1));
			}
		});
		
		this.solutionTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.matches("[0]+[1-9]")) {
				newValue = newValue.replaceFirst("^0+(?!$)", "");
				this.solutionTextField.setText(newValue);
			}
			if (!newValue.matches("|[0]{1}|[1-9]+|[1-9]+[0]+|[1-9]+[0]+[1-9]+")) {
				this.solutionTextField.setText(oldValue);
			}
		});
	}
	
	private void setupListenersForListView() {
		this.playerListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				this.nameTextField.textProperty().set(newValue.getName());
			}
		});
	}
	
	private void setupSelectionHandler() {
		this.difficultyComboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				if (newValue == Difficulty.EASY) {
					this.viewModel.setDifficulty();
					this.solutionTextField.setText("");
				}
				if (newValue == Difficulty.MEDIUM) {
					this.viewModel.setDifficulty();
					this.solutionTextField.setText("");
				}
				if (newValue == Difficulty.HARD) {
					this.viewModel.setDifficulty();
					this.solutionTextField.setText("");
				}
			}
		});
	}

	@FXML
	void handleNext(ActionEvent event) {
		this.viewModel.next();
		this.solutionTextField.setText("");
	}

	@FXML
	void handleStart(ActionEvent event) {
		this.viewModel.initializeFields();
		this.solutionTextField.setText("");
	}

	@FXML
	void handleSubmit(ActionEvent event) {
		this.viewModel.submit();
	}
	
	@FXML
    void handleSave(ActionEvent event) {
		this.viewModel.save();
    }

}
