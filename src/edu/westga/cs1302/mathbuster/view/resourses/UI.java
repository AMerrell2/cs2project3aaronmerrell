/**
 * 
 */
package edu.westga.cs1302.mathbuster.view.resourses;

/**
 * The class UI.
 * 
 * @author Aaron Merrell
 *
 */
public final class UI {
	
	/**
	 * Defines string messages for exception messages for math buster.
	 * 
	 * @author Aaron Merrell
	 *
	 */
	public static final class ExceptionMessages {
		public static final String EMPTY_NAME = "Name cannot be empty.";
		public static final String OPERAND_OUT_OF_BOUNDS = "Operand must be at least 0 and less than 1000.";
	}
	
	/**
	 * Defines repeated string values.
	 * 
	 * @author Aaron Merrell
	 *
	 */
	public static final class Utility {
		public static final String PLUS_SIGN = " + ";
		public static final String MINUS_SIGN = " - ";
		public static final String EQUAL_SIGN = " = ";
		public static final String PLAYER_FILE = "TextAudioFiles/Players.txt";
		
	}
}
